#!/usr/bin/bash
set -x

# Signal build requirements
echo "Entering /Signal-Desktop"
pushd /Signal-Desktop
git-lfs install
git config --global user.name name
git config --global user.email name@example.com
# Drop "--no-sandbox" commit from build
git revert 1ca0d821078286d5953cf0d598e6b97710f816ef
# The mock tests are broken on custom arm builds
sed -r '/mock/d' -i package.json
# Dry run
# This may have to be cancelled and run again to get it to actually rebuild deps...
npm install # not recommended by signal, but required due to those two sed lines.
npm install --frozen-lockfile
rm -rf ts/test-mock # also broken on arm64
npm run generate
#yarn test always fails on arm...
npm run build:release --arm64 --linux --dir
npm run build:release --arm64 --linux deb
popd

pushd /Signal-Desktop/release
mv linux-arm64-unpacked signal
tar cJvf signal-desktop_$(grep version ../package.json  | awk '{ print $2 }' | tr -d '",' | head -n 1).tar.xz signal
